const db_connection = require("./src/model");
const User = require('./src/model/model')

User.sync()
.then(()=>{
    console.log('Model created')
})
.catch((error)=>{
    console.log('While creating models error occurred: ', error)
})

db_connection.authenticate()
.then(()=>{
    console.log('connection ready')
})
.catch((err)=>{
    console.log('error occured: ', err)
})

