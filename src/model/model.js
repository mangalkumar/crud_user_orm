const Sequelize = require('sequelize')
const db_connection = require('./index')

const User = db_connection.define("user", {
  firstName: {
    type: Sequelize.STRING
  },
  lastName: {
    type: Sequelize.STRING
  },
  email: {
    type: Sequelize.STRING
  },
  password: {
    type: Sequelize.STRING
  },
  address: {
    type: Sequelize.STRING
  },
  phoneNumber: {
    type: Sequelize.STRING
  }
})

module.exports = User
