const env = require("../../config.js")

const Sequelize = require("sequelize")

const db_connection = new Sequelize(env.DB_NAME, env.DB_USER, env.DB_PASS, {
    host: env.DB_HOST,
    dialect: env.DIALECT,
})


module.exports = db_connection