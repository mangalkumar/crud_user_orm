const User = require('../model/model')

// Create and Save a new User
exports.create = (dataObj) => {
    if (!dataObj.firstName) {
        console.log({
            message: "First name can not be empty!"
        });
        return;
    } else if (!dataObj.email) {
        console.log({
            message: "Email id can not be empty!"
        });
        return;
    } else if (!dataObj.password) {
        console.log({
            message: "Password can not be empty!"
        });
        return;
    } else if (!dataObj.address) {
        console.log({
            message: "Address can not be empty!"
        });
        return;
    } else if (!dataObj.phoneNumber) {
        console.log({
            message: "Phone Number can not be empty!"
        });
        return;
    }

    // Create a User
    const user = {
        firstName: dataObj.firstName,
        lastName: dataObj.lastName,
        email: dataObj.email,
        password: dataObj.password,
        address: dataObj.address,
        phoneNumber: dataObj.phoneNumber
    }

    // Save User in the database
    User.create(user)
        .then(data => {
            console.log('Data inserted to the table', data)
        })
        .catch(error => {
            console.log("While inserting data error occurred: ", error)
        });
};

// Retrieve all Users from the database.
exports.findAll = () => {

    User.findAll()
        .then(data => {
            console.log(data)
        })
        .catch(err => {
            console.log("While fetching data error occurred: ", err)
        });
};

// Find a single User with an id
exports.findById = (id) => {
    User.findByPk(id)
        .then(data => {
            console.log(data);
        })
        .catch(err => {
            console.log("While searching user, error occurred : ", err)
        });
};

// Update a User by the id in the request
exports.update = (id, data) => {

    User.update(data, {
        where: { id: id }
    })
    .then(num => {
        if (num == 1) {
            console.log("User was updated successfully.")
        } else {
            console.log(`Cannot update User with id=${id}. User was not found or passed invalid data`)
        }
    })
    .catch(err => {
        console.log({
            message: "Error updating User with id=" + id
        });
    });
};

// Delete a User with the specified id in the request
exports.deleteById = (id) => {

    User.destroy({
        where: { id: id }
    })
    .then(num => {
        if (num == 1) {
            console.log(`User was deleted successfully!`)
        } else {
            console.log({
                message: `Cannot delete User with id=${id}. Maybe User was not found!`
            });
        }
    })
    .catch(err => {
        console.log('While deleting user some error occurred.', err)
    });
};
