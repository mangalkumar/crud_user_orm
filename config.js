const dotenv = require('dotenv')

dotenv.config()

module.exports = {
    DB_NAME: process.env.DB_NAME,
    DB_HOST: process.env.DB_HOST,
    DB_USER: process.env.DB_USER,
    DB_PASS: process.env.DB_PASS,
    DIALECT: process.env.DIALECT
}